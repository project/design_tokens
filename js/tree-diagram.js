(function (Drupal, once, go) {

  function init(target) {
    const $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram =
      new go.Diagram(target.id, // must be the ID or reference to div
        {
          allowCopy: false,
          allowDelete: false,
          initialAutoScale: go.AutoScale.UniformToFill,
          maxSelectionCount: 1, // users can select only one part at a time
          validCycle: go.CycleMode.DestinationTree, // make sure users can only create trees
          'clickCreatingTool.archetypeNodeData': { // allow double-click in background to create a new node
            name: '(New person)',
            title: '(Title)',
            dept: '(Dept)',
          },
          'clickCreatingTool.insertPart': function (loc) {  // method override must be function, not =>
            const node = go.ClickCreatingTool.prototype.insertPart.call(this, loc);
            if (node !== null) {
              this.diagram.select(node);
              this.diagram.commandHandler.scrollToPart(node);
              this.diagram.commandHandler.editTextBlock(node.findObject('NAMETB'));
            }
            return node;
          },
          layout:
            $(go.TreeLayout,
              {
                treeStyle: go.TreeStyle.LastParents,
                arrangement: go.TreeArrangement.Horizontal,
                // properties for most of the tree:
                angle: 90,
                layerSpacing: 35,
                // properties for the "last parents":
                alternateAngle: 90,
                alternateLayerSpacing: 35,
                alternateAlignment: go.TreeAlignment.Bus,
                alternateNodeSpacing: 20,
              }),
          'undoManager.isEnabled': true, // enable undo & redo
          'themeManager.changesDivBackground': true,
          'themeManager.currentTheme': 'light',
        });

    // when the document is modified, add a "*" to the title and enable the "Save" button
    myDiagram.addDiagramListener('Modified', e => {
      const button = document.getElementById('SaveButton');
      if (button) {
        button.disabled = !myDiagram.isModified;
      }
      const idx = document.title.indexOf('*');
      if (myDiagram.isModified) {
        if (idx < 0) {
          document.title += '*';
        }
      } else {
        if (idx >= 0) {
          document.title = document.title.slice(0, idx);
        }
      }
    });

    // set up some colors/fonts for the default ('light') and dark Themes
    myDiagram.themeManager.set('light', {
      colors: {
        background: '#fff',
        text: '#111827',
        textHighlight: '#11a8cd',
        subtext: '#6b7280',
        badge: '#f0fdf4',
        badgeBorder: '#16a34a33',
        badgeText: '#15803d',
        divider: '#6b7280',
        shadow: '#9ca3af',
        tooltip: '#1f2937',
        levels: [
          '#ac193d', '#2672ec', '#8c0095', '#5133ab',
          '#008299', '#d24726', '#008a00', '#094ab2',
        ],
        dragOver: '#f0f9ff',
        link: '#9ca3af',
        div: '#f3f4f6',
      },
      fonts: {
        name: '500 0.875rem Inter, sans-serif',
        normal: '0.875rem Inter, sans-serif',
        badge: '500 0.75rem Inter, sans-serif',
        link: '600 0.875rem Inter, sans-serif',
      },
    });

    // this is used to determine feedback during drags
    function mayWorkFor(node1, node2) {
      if (!(node1 instanceof go.Node)) {
        return false;
      }  // must be a Node
      if (node1 === node2) {
        return false;
      }  // cannot work for yourself
      if (node2.isInTreeOf(node1)) {
        return false;
      }  // cannot work for someone who works for you
      return true;
    }

    // This converter is used by the Picture.
    function findHeadShot(pic) {
      if (!pic) {
        return '../samples/images/user.svg';
      } // There are only 16 images on the server
      return '../samples/images/HS' + pic;
    }

    // Used to convert the node's tree level into a theme color
    function findLevelColor(node) {
      return node.findTreeLevel();
    }

    // Gets the text for a tooltip based on the adorned object's name
    function toolTipTextConverter(obj) {
      if (obj.name === 'EMAIL') {
        return obj.part.data.email;
      }
      if (obj.name === 'PHONE') {
        return obj.part.data.phone;
      }
    }

    // Align the tooltip based on the adorned object's viewport bounds
    function toolTipAlignConverter(obj, tt) {
      const d = obj.diagram;
      const bot = obj.getDocumentPoint(go.Spot.Bottom);
      const viewPt = d.transformDocToView(bot).offset(0, 35);
      // if tooltip would be below viewport, show above instead
      const align = d.viewportBounds.height >= viewPt.y / d.scale ? new go.Spot(0.5, 1, 0, 6) : new go.Spot(0.5, 0, 0, -6);

      tt.alignment = align;
      tt.alignmentFocus = align.y === 1 ? go.Spot.Top : go.Spot.Bottom;
    }

    // a tooltip for the Email and Phone buttons
    const toolTip =
      new go.Adornment(go.Panel.Spot, { isShadowed: true, shadowOffset: new go.Point(0, 2) }).add(
        new go.Placeholder(),
        new go.Panel(go.Panel.Auto)
          .add(
            new go.Shape('RoundedRectangle', { strokeWidth: 0, shadowVisible: true })
              .theme('fill', 'background'),
            new go.TextBlock({ margin: 2 })
              .bindObject('text', 'adornedObject', toolTipTextConverter)
              .theme('stroke', 'text')
              .theme('font', 'normal'),
          )
          // sets alignment and alignmentFocus based on adorned object's position in viewport
          .bindObject('', 'adornedObject', toolTipAlignConverter),
      )
        .theme('shadowColor', 'shadow');

    // define the Node template
    myDiagram.nodeTemplate =
      new go.Node(go.Panel.Spot, {
        isShadowed: true,
        shadowOffset: new go.Point(0, 2),
        selectionObjectName: 'BODY',
        // show/hide buttons when mouse enters/leaves
        mouseEnter: (e, node) => node.findObject('BUTTON').opacity = node.findObject('BUTTONX').opacity = 1,
        mouseLeave: (e, node) => node.findObject('BUTTON').opacity = node.findObject('BUTTONX').opacity = 0,
        // handle dragging a Node onto a Node to (maybe) change the reporting relationship
        mouseDragEnter: (e, node, prev) => {
          const diagram = node.diagram;
          const selnode = diagram.selection.first();
          if (!mayWorkFor(selnode, node)) {
            return;
          }
          const shape = node.findObject('SHAPE');
          if (shape) {
            shape._prevFill = shape.fill;  // remember the original brush
            shape.fill = diagram.themeManager.findValue('dragOver', 'colors'); // "#e0f2fe";
          }
        },
        mouseDragLeave: (e, node, next) => {
          const shape = node.findObject('SHAPE');
          if (shape && shape._prevFill) {
            shape.fill = shape._prevFill;  // restore the original brush
          }
        },
        mouseDrop: (e, node) => {
          const diagram = node.diagram;
          const selnode = diagram.selection.first();  // assume just one Node in selection
          if (mayWorkFor(selnode, node)) {
            // find any existing link into the selected node
            const link = selnode.findTreeParentLink();
            if (link !== null) {  // reconnect any existing link
              link.fromNode = node;
            } else {  // else create a new link
              diagram.toolManager.linkingTool.insertLink(node, node.port, selnode, selnode.port);
            }
          }
        },
      })
        .add(
          new go.Panel(go.Panel.Auto, { name: 'BODY' }).add(
            // define the node's outer shape
            new go.Shape('RoundedRectangle',
              { name: 'SHAPE', strokeWidth: 0, portId: '', spot1: go.Spot.TopLeft, spot2: go.Spot.BottomRight })
              .theme('fill', 'background'),
            new go.Panel(go.Panel.Table, { margin: 0.5, defaultRowSeparatorStrokeWidth: 0.5 })
              .theme('defaultRowSeparatorStroke', 'divider')
              .add(
                new go.Panel(go.Panel.Table, { padding: new go.Margin(18, 18, 18, 24) })
                  .addColumnDefinition(0, { width: 240 })
                  .add(
                    new go.Panel(go.Panel.Table, { column: 0, alignment: go.Spot.Left, stretch: go.Stretch.Vertical, defaultAlignment: go.Spot.Left }).add(
                      new go.Panel(go.Panel.Horizontal, { row: 0 }).add(
                        new go.TextBlock({ editable: true, minSize: new go.Size(10, 14) })
                          .bindTwoWay('text', 'name')
                          .theme('stroke', 'text')
                          .theme('font', 'name'),
                      ),
                      new go.TextBlock({ row: 1, editable: true, minSize: new go.Size(10, 14), margin: new go.Margin(5, 0, 0, 0) })
                        .bindTwoWay('text', 'title')
                        .theme('stroke', 'subtext')
                        .theme('font', 'normal'),
                      new go.TextBlock({ row: 2, editable: true, minSize: new go.Size(10, 14), margin: new go.Margin(10, 0, 0, 0) })
                        .bindTwoWay('text', 'dept')
                        .theme('fill', 'badge')
                        .theme('stroke', 'badgeText')
                        .theme('font', 'monospace'),
                    ),
                  ),
                new go.Panel(go.Panel.Table, { row: 1, stretch: go.Stretch.Horizontal, defaultColumnSeparatorStrokeWidth: 0.5 })
                  .theme('defaultColumnSeparatorStroke', 'divider')
                  .add(
                    makeBottomButton('EMAIL'),
                    makeBottomButton('PHONE'),
                  ),
              ),
          ),  // end Auto Panel
          new go.Shape('RoundedLeftRectangle', {
            alignment: go.Spot.Left, alignmentFocus: go.Spot.Left,
            stretch: go.Stretch.Vertical, width: 6, strokeWidth: 0,
          })
            .themeObject('fill', '', 'levels', findLevelColor),
          $('Button',
            $(go.Shape, 'PlusLine', { width: 8, height: 8, stroke: '#0a0a0a', strokeWidth: 2 }),
            {
              name: 'BUTTON', alignment: go.Spot.Right, opacity: 0,  // initially not visible
              click: (e, button) => addEmployee(button.part),
            },
            // button is visible either when node is selected or on mouse-over
            new go.Binding('opacity', 'isSelected', s => s ? 1 : 0).ofObject(),
          ),
          $('TreeExpanderButton',
            {
              '_treeExpandedFigure': 'LineUp', '_treeCollapsedFigure': 'LineDown',
              name: 'BUTTONX', alignment: go.Spot.Bottom, opacity: 0,  // initially not visible
            },
            // button is visible either when node is selected or on mouse-over
            new go.Binding('opacity', 'isSelected', s => s ? 1 : 0).ofObject(),
          ),
        )
        .theme('shadowColor', 'shadow')
        // for sorting, have the Node.text be the data.name
        .bind('text', 'name')
        // bind the Part.layerName to control the Node's layer depending on whether it isSelected
        .bindObject('layerName', 'isSelected', sel => sel ? 'Foreground' : '')
        .bindTwoWay('isTreeExpanded');

    function makeBottomButton(name) {
      const emailPath = 'F M328.883,89.125l107.59,107.589l-272.34,272.34L56.604,361.465L328.883,89.125z M518.113,63.177l-47.981-47.981 c-18.543-18.543-48.653-18.543-67.259,0l-45.961,45.961l107.59,107.59l53.611-53.611 C532.495,100.753,532.495,77.559,518.113,63.177z M0.3,512.69c-1.958,8.812,5.998,16.708,14.811,14.565l119.891-29.069 L27.473,390.597L0.3,512.69z';
      const phonePath = 'F M591.657,1109.24 C592.048,1109.63 592.048,1110.27 591.657,1110.66 C591.267,1111.05 590.633,1111.05 590.242,1110.66 L586.006,1106.42 L581.74,1110.69 C581.346,1111.08 580.708,1111.08 580.314,1110.69 C579.921,1110.29 579.921,1109.65 580.314,1109.26 L584.58,1104.99 L580.344,1100.76 C579.953,1100.37 579.953,1099.73 580.344,1099.34 C580.733,1098.95 581.367,1098.95 581.758,1099.34 L585.994,1103.58 L590.292,1099.28 C590.686,1098.89 591.323,1098.89 591.717,1099.28 C592.11,1099.68 592.11,1100.31 591.717,1100.71 L587.42,1105.01 L591.657,1109.24 L591.657,1109.24 Z M586,1089 C577.163,1089 570,1096.16 570,1105 C570,1113.84 577.163,1121 586,1121 C594.837,1121 602,1113.84 602,1105 C602,1096.16 594.837,1089 586,1089 L586,1089 Z';
      const convertSelectedToThemeProp = s => s ? 'textHighlight' : 'text';
      const isEmail = name === 'EMAIL';
      return new go.Panel(go.Panel.Table,
        {
          mouseEnter: (e, obj) => myDiagram.model.set(obj.part.data, name, true),
          mouseLeave: (e, obj) => myDiagram.model.set(obj.part.data, name, false),
          name, background: 'transparent',
          cursor: 'Pointer',
          column: isEmail ? 0 : 1,
          width: 140, height: 40,
          toolTip: toolTip,
          click: (e, obj) => {
            alert(`You clicked to ${isEmail ? 'send email to' : 'call'} ${obj.part.data.name} at ${obj.part.data[name.toLowerCase()]}`);
          },
        })
        .add(
          new go.Panel(go.Panel.Horizontal)
            .add(
              new go.Shape(
                {
                  geometryString: isEmail ? emailPath : phonePath,
                  strokeWidth: 0,
                  desiredSize: isEmail ? new go.Size(20, 16) : new go.Size(20, 20),
                  margin: new go.Margin(0, 12, 0, 0),
                })
                .theme('fill', 'text')
                .themeData('fill', name, null, convertSelectedToThemeProp)
              ,
              new go.TextBlock(isEmail ? 'Edit' : 'Delete')
                .theme('stroke', 'text')
                .themeData('stroke', name, null, convertSelectedToThemeProp)
                .theme('font', 'link'),
            ),
        );
    }

    function addEmployee(node) {
      if (!node) {
        return;
      }
      const thisemp = node.data;
      let newnode;
      myDiagram.model.commit(m => {
        const newemp = { name: '(New person)', title: '(Title)', dept: thisemp.dept, parent: thisemp.key };
        m.addNodeData(newemp);
        newnode = myDiagram.findNodeForData(newemp);
        // set location so new node doesn't animate in from top left
        if (newnode) {
          newnode.location = node.location;
        }
      }, 'add employee');
      myDiagram.commandHandler.scrollToPart(newnode);
    }

    // the context menu allows users to make a position vacant,
    // remove a role and reassign the subtree, or remove a department
    myDiagram.nodeTemplate.contextMenu =
      $('ContextMenu',
        $('ContextMenuButton',
          $(go.TextBlock, 'Add Employee'),
          {
            click: (e, button) => addEmployee(button.part.adornedPart),
          },
        ),
        $('ContextMenuButton',
          $(go.TextBlock, 'Vacate Position'),
          {
            click: (e, button) => {
              const node = button.part.adornedPart;
              if (node !== null) {
                const thisemp = node.data;
                myDiagram.model.commit(m => {
                  // update the name, picture, email, and phone, but leave the title/department
                  m.set(thisemp, 'name', '(Vacant)');
                  m.set(thisemp, 'pic', '');
                  m.set(thisemp, 'email', 'none');
                  m.set(thisemp, 'phone', 'none');
                }, 'vacate');
              }
            },
          },
        ),
        $('ContextMenuButton',
          $(go.TextBlock, 'Remove Role'),
          {
            click: (e, button) => {
              // reparent the subtree to this node's boss, then remove the node
              const node = button.part.adornedPart;
              if (node !== null) {
                myDiagram.model.commit(m => {
                  const chl = node.findTreeChildrenNodes();
                  // iterate through the children and set their parent key to our selected node's parent key
                  while (chl.next()) {
                    const emp = chl.value;
                    m.setParentKeyForNodeData(emp.data, node.findTreeParentNode().data.key);
                  }
                  // and now remove the selected node itself
                  m.removeNodeData(node.data);
                }, 'reparent remove');
              }
            },
          },
        ),
        $('ContextMenuButton',
          $(go.TextBlock, 'Remove Department'),
          {
            click: (e, button) => {
              // remove the whole subtree, including the node itself
              const node = button.part.adornedPart;
              if (node !== null) {
                myDiagram.commit(d => d.removeParts(node.findTreeParts()), 'remove dept');
              }
            },
          },
        ),
      );

    // define the Link template
    myDiagram.linkTemplate =
      $(go.Link,
        { routing: go.Routing.Orthogonal, layerName: 'Background', corner: 5 },
        $(go.Shape, { strokeWidth: 2 },
          new go.ThemeBinding('stroke', 'link'),
        ));  // the link shape

    // read in the JSON-format data from the "edit-data" element
    load();

    // // support editing the properties of the selected person in HTML
    // myInspector = new Inspector('myInspector', myDiagram,
    //   {
    //     properties: {
    //       'key': { readOnly: true },
    //       // Don't show these temporary data values
    //       'EMAIL': { show: false },
    //       'PHONE': { show: false },
    //     },
    //   });
    //
    // // Setup zoom to fit button
    // document.getElementById('zoomToFit').addEventListener('click', () => myDiagram.commandHandler.zoomToFit());
    //
    // document.getElementById('centerRoot').addEventListener('click', () => {
    //   myDiagram.scale = 1;
    //   myDiagram.commandHandler.scrollToPart(myDiagram.findNodeForKey(1));
    // });
  } // end init

  // Show the diagram's model in JSON format
  function save() {
    document.getElementById('edit-data').value = myDiagram.model.toJson();
    myDiagram.isModified = false;
  }

  function load() {
    myDiagram.model = go.Model.fromJson(document.getElementById('edit-data').value);
    // make sure new data keys are unique positive integers
    let lastkey = 1;
    myDiagram.model.makeUniqueKeyFunction = (model, data) => {
      let k = data.key || lastkey;
      while (model.findNodeDataForKey(k)) {
        k++;
      }
      data.key = lastkey = k;
      return k;
    };
  }

  function changeTheme() {
    const myDiagram = go.Diagram.fromDiv('myDiagramDiv');
    if (myDiagram) {
      myDiagram.themeManager.currentTheme = document.getElementById('theme').value;
    }
  }

  Drupal.behaviors.treeDiagram = {
    attach(context, settings) {
      const wrapper = once('design-tokens-tree-diagram', '#myDiagramDiv', context)[0];
      if (!wrapper) {
        return;
      }
      setTimeout(() => {
        init(wrapper);
      }, 300);
    },
  };

})(Drupal, once, go);

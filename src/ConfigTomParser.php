<?php

namespace Drupal\design_tokens;

use Drupal\design_tokens\tom\DesignToken;
use Drupal\design_tokens\tom\Group;
use Drupal\design_tokens\tom\RootGroup;

class ConfigTomParser {

  public function __construct() {}

  public function parse(): RootGroup {
    $root = new RootGroup();
    $tokens = \Drupal::entityTypeManager()
      ->getStorage('design_token')
      ->loadMultiple();
    $this->doParseDTCG($tokens, $root);
    return $root;
  }

  protected function doParseDTCG(array $tokens, Group $root): void {
    /**
     * @var string $id
     * @var \Drupal\design_tokens\DesignTokenInterface $token
     */
    // @todo We are not handling groups yet.
    foreach ($tokens as $id => $token) {
      if ($token->isPrimitive()) {
        $value = $token->getValue();
      }
      else {
        $referencedToken = $token->getReferencedToken();
        if (!$referencedToken) {
          continue;
        }
        $value = '{' . $referencedToken->label() . '}';
      }
      $node = new DesignToken($token->label(), $token->getType(), $value, $token->getDescription());
      $root->addChild($node);
    }
  }

}

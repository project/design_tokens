<?php

namespace Drupal\design_tokens\Drush\Commands;

use Drupal\design_tokens\Importer\ComponentTokensImporter;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 */
final class DesignTokensCommands extends DrushCommands {

  /**
   * Constructs a DesignTokensCommands object.
   */
  public function __construct(
    private readonly ComponentTokensImporter $tokensImporter,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(ComponentTokensImporter::class),
    );
  }

  /**
   * Command description here.
   */
  #[CLI\Command(name: 'design_tokens:import', aliases: ['design-tokens-import'])]
  #[CLI\Argument(name: 'componentId', description: 'Comma separated list of Component IDs. Leave empty to scan all the components in the site.')]
  #[CLI\Usage(name: 'design_tokens:import', description: 'Imports all the design tokens for all the components in the site.')]
  #[CLI\Usage(name: 'design_tokens:import sdc_examples:my-banner', description: 'Imports all the design tokens for all the component My Banner provided by the SDC Examples module.')]
  public function import(string $componentId = NULL): void {
    $componentIds = array_filter(explode(',', $componentId));
    $total = $this->tokensImporter->importTokens($componentIds);
    $message = dt('%total design tokens imported', ['%total' => $total]);
    $this->logger()?->success($message);
  }

}

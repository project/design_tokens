<?php

declare(strict_types=1);

namespace Drupal\design_tokens;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\design_tokens\Entity\DesignToken;

/**
 * Custom storage for design tokens.
 */
final class DesignTokenStorage extends ConfigEntityStorage {

  public function loadDesignTokenTree(DesignTokenInterface $design_token): DesignTokenTree {
    // Create a token tree from a single token to initialize the process.
    $token_tree = new DesignTokenTree($design_token);
    return $this->doLoadDesignTokenTree($token_tree);
  }

  private function doLoadDesignTokenTree(DesignTokenTree $design_token_tree): DesignTokenTree {
    $token_id = $design_token_tree->designToken->id();
    $referring_tokens = array_values(
      $this->loadByProperties(['value' => $token_id])
    );
    $referring_token_trees = array_map(
      static fn (DesignToken $token) => new DesignTokenTree($token),
      $referring_tokens,
    );
    return array_reduce(
     $referring_token_trees,
     function(DesignTokenTree $parent, DesignTokenTree $child) {
       $child = $child->designToken->isComponent()
         ? $child
         : $this->doLoadDesignTokenTree($child);
        $parent->addSubtree($child);
        return $parent;
     },
     $design_token_tree,
    );
  }

}

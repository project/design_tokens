<?php

declare(strict_types=1);

namespace Drupal\design_tokens\Plugin\jsonrpc\Method;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\design_tokens\DesignTokenStorage;
use Drupal\design_tokens\Entity\DesignToken;
use Drupal\jsonrpc\Annotation\JsonRpcParameterDefinition;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\Handler;
use Drupal\jsonrpc\HandlerInterface;
use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Object\Error;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc\Object\Response;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Lists design tokens.
 *
 * @JsonRpcMethod(
 *   id = "design_tokens.save_multiple",
 *   usage = @Translation("Save multiple design token values"),
 *   access = {"administer design_token"},
 *   params = {
 *     "tokens" = @JsonRpcParameterDefinition(factory = "\Drupal\design_tokens\ParameterFactory\DesignTokenValuesParameterFactory"),
 *     "roots" = @JsonRpcParameterDefinition(factory = "\Drupal\design_tokens\ParameterFactory\DesignTokenValuesParameterFactory"),
 *   }
 * )
 */
class DesignTokensSaveMultiple extends JsonRpcMethodBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private readonly EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, string $plugin_id, MethodInterface $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\jsonrpc\Exception\JsonRpcException
   */
  public function execute(ParameterBag $params): Response {
    $design_tokens = $params->get('tokens');
    // Now delete connections that are not present in the tree.
    $primitive_token = array_filter($design_tokens, static fn(DesignToken $token) => $token->isPrimitive());
    // There should only be one primitive token, but let's loop just in case.
    try {
      $roots = $params->get('roots');
      array_map(
        fn (DesignToken $token) => $this->deleteMissingConnections(
          $token->id(),
          $design_tokens,
        ),
        $roots,
      );
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
      throw JsonRpcException::fromError(Error::internalError($e->getMessage()));
    }
    try {
      array_map(
        static fn(DesignToken $token) => $token->save(),
        $design_tokens,
      );
    }
    catch (EntityStorageException $e) {
      throw JsonRpcException::fromError(Error::internalError($e->getMessage()));
    }
    $response = new Response(
      version: Handler::SUPPORTED_VERSION,
      id: $this->currentRequest()->id(),
      result: 'Success',
    );

    // Make it uncacheable.
    $metadata = new CacheableMetadata();
    $metadata->setCacheMaxAge(0);
    $response->addCacheableDependency($metadata);
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public static function outputSchema(): array {
    return ['type' => 'string'];
  }

  /**
   * Deletes connections to missing nodes in the input.
   *
   * @param string $root_id
   *   The primitive token, root of the tree.
   * @param array $provided_tokens
   *   The tokens in the payload.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function deleteMissingConnections(string $root_id, array $provided_tokens): void {
    $storage = $this->entityTypeManager->getStorage('design_token');
    assert($storage instanceof DesignTokenStorage);
    $token = $storage->load($root_id);
    if (!$token instanceof DesignToken || !$token->isPrimitive()) {
      return;
    }
    $full_tree = $storage->loadDesignTokenTree($token);
    $all_tokens = array_filter(
      $full_tree->getAllTokens(),
      static fn (DesignToken $token) => !$token->isPrimitive(),
    );
    $provided_token_ids = array_map(
      static fn(DesignToken $token) => $token->id(),
      $provided_tokens,
    );
    $all_token_ids = array_map(
      static fn(DesignToken $token) => $token->id(),
      $all_tokens,
    );
    $missing_token_ids = array_diff($all_token_ids, $provided_token_ids);
    array_map(
      function(DesignToken $token) use ($missing_token_ids) {
        if (in_array($token->id(), $missing_token_ids, TRUE)) {
          $token->set('value', NULL);
          $token->save();
        }
      },
      $all_tokens,
    );

  }

}

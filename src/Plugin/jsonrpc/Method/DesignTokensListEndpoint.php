<?php

declare(strict_types=1);

namespace Drupal\design_tokens\Plugin\jsonrpc\Method;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\design_tokens\Entity\DesignToken;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\Handler;
use Drupal\jsonrpc\HandlerInterface;
use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Object\Error;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc\Object\Response;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Lists design tokens.
 *
 * @JsonRpcMethod(
 *   id = "design_tokens.list",
 *   usage = @Translation("Lists all design tokens for the selected layer"),
 *   access = {"administer design_token"},
 *   params = {
 *     "layers" = @JsonRpcParameterDefinition(schema = {"type": "array", "items": {"type": "string"}}),
 *   }
 * )
 */
class DesignTokensListEndpoint extends JsonRpcMethodBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private readonly EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, string $plugin_id, MethodInterface $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\jsonrpc\Exception\JsonRpcException
   */
  public function execute(ParameterBag $params): Response {
    $layers = $params->get('layers');
    // Validate we got a valid layer.
    $valid_layers = [
      DesignToken::LAYER_PRIMITIVE,
      DesignToken::LAYER_SEMANTIC,
      DesignToken::LAYER_COMPONENT,
    ];
    $invalid_layers = array_diff($layers, $valid_layers);
    if ($invalid_layers) {
      $message = sprintf('Invalid parameter "%s" for the component layers. The allowed values are: %s.', implode(', ', $invalid_layers), implode(', ', $valid_layers));
      throw JsonRpcException::fromError(Error::invalidParams($message));
    }
    try {
      $storage = $this->entityTypeManager->getStorage('design_token');
    }
    catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      throw JsonRpcException::fromError(Error::internalError($e->getMessage()));
    }
    $design_tokens = array_values($storage->loadByProperties(['layer' => $layers]));
    $output = array_map(
      fn(DesignToken $token) => $this->normalizeToken($token),
      $design_tokens
    );
    $response = new Response(
      version: Handler::SUPPORTED_VERSION,
      id: $this->currentRequest()->id(),
      result: $output,
    );
    $entity_type = $storage->getEntityType();
    $response->addCacheTags($entity_type->getListCacheTags());
    $response->addCacheContexts($entity_type->getListCacheContexts());
    return array_reduce(
      $design_tokens,
      static fn(Response $response, DesignToken $token) => $response->addCacheableDependency($token),
      $response,
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function outputSchema(): array {
    return [
      'type' => 'array',
      'items' => [
        'type' => 'object',
        'required' => ['id', 'uuid', 'layer', 'name', 'value'],
        'properties' => [
          'id' => ['type' => 'string'],
          'uuid' => ['type' => 'string'],
          'name' => ['type' => 'string'],
          'label' => ['type' => ['string', 'null']],
          'description' => ['type' => ['string', 'null']],
          'type' => ['type' => ['string', 'null']],
          'value' => ['type' => ['string', 'object', 'number', 'null']],
          'layer' => ['type' => 'string'],
          'needsSiteBuilderReview' => ['type' => 'boolean'],
          'autoCreatedFrom' => ['type' => ['string', 'null']],
        ],
      ],
    ];
  }

  /**
   * Normalizes a design token for JSON encoding.
   *
   * @param \Drupal\design_tokens\Entity\DesignToken $token
   *   The design token.
   *
   * @return array
   *   The array ready for JSON encoding
   */
  private function normalizeToken(DesignToken $token): array {
    $schema = $this::outputSchema();
    $properties = array_keys($schema['items']['properties']);
    $normalized = $token->toArray();
    // Only output the properties we care about.
    return array_intersect_key($normalized, array_flip($properties));
  }

}

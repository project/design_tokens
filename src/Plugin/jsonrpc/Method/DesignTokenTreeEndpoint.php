<?php

declare(strict_types=1);

namespace Drupal\design_tokens\Plugin\jsonrpc\Method;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\design_tokens\DesignTokenStorage;
use Drupal\design_tokens\DesignTokenTree;
use Drupal\design_tokens\Entity\DesignToken;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\Handler;
use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Object\Error;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc\Object\Response;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Lists design tokens.
 *
 * @JsonRpcMethod(
 *   id = "design_token_tree",
 *   usage = @Translation("Lists all design tokens for the selected layer"),
 *   access = {"administer design_token"},
 *   params = {
 *     "tokenId" = @JsonRpcParameterDefinition(schema = {"type": "string"}),
 *   }
 * )
 */
class DesignTokenTreeEndpoint extends JsonRpcMethodBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private readonly EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, string $plugin_id, MethodInterface $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\jsonrpc\Exception\JsonRpcException
   */
  public function execute(ParameterBag $params): Response {
    $token_id = $params->get('tokenId');
    try {
      $storage = $this->entityTypeManager->getStorage('design_token');
    }
    catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      throw JsonRpcException::fromError(Error::invalidParams($e->getMessage()));
    }
    $token = $storage->load($token_id);
    if (!$token instanceof DesignToken || !$storage instanceof DesignTokenStorage) {
      $message = sprintf('Unable to find a design token with ID "%s".', $token_id);
      throw JsonRpcException::fromError(Error::invalidParams($message));
    }
    // Validate that it's a primitive component.
    if (!$token->isPrimitive()) {
      $message = sprintf('Load the token tree from the primitive token.');
      throw JsonRpcException::fromError(Error::invalidParams($message));
    }
    $full_tree = $storage->loadDesignTokenTree($token);
    $response = new Response(
      version: Handler::SUPPORTED_VERSION,
      id: $this->currentRequest()->id(),
      result: $this->normalizeTokenTree($full_tree),
    );
    $all_tokens = $full_tree->getAllTokens();
    return array_reduce(
      $all_tokens,
      static function (Response $response, DesignToken $child): Response {
        $response->addCacheableDependency($child);
        return $response;
      },
      $response,
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function outputSchema(): array {
    return [
      'type' => 'object',
    ];
  }

  /**
   * Normalizes a design token tree for JSON encoding.
   *
   * @param \Drupal\design_tokens\DesignTokenTree $design_token_tree
   *   The token tree.
   *
   * @return array
   *   The array ready for JSON encoding
   */
  private function normalizeTokenTree(DesignTokenTree $design_token_tree): array {
    return [
      'design_token' => $this->normalizeToken($design_token_tree->designToken),
      'children' => array_map(
        fn (DesignTokenTree $child_tree) => $this->normalizeTokenTree($child_tree),
        $design_token_tree->getSubtrees(),
      ),
    ];
  }

  /**
   * Normalizes a design token for JSON encoding.
   *
   * @param \Drupal\design_tokens\Entity\DesignToken $design_token
   *   The design token.
   *
   * @return array
   *   The array ready for JSON encoding
   */
  private function normalizeToken(DesignToken $design_token): array {
    $schema = DesignTokensListEndpoint::outputSchema();
    $properties = array_keys($schema['items']['properties']);
    $normalized = $design_token->toArray();
    // Only output the properties we care about.
    return array_intersect_key($normalized, array_flip($properties));
  }
}

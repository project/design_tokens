<?php

namespace Drupal\design_tokens\Controllers;

use Drupal\Core\Controller\ControllerBase;
use Drupal\design_tokens\ConfigTomParser;
use Drupal\design_tokens\tom\Serializer\CssVarsSerializer;
use Symfony\Component\HttpFoundation\Response;

class LibraryStyleController extends ControllerBase {

  public function styles() {
    // Use the DTCG formatted design.tokens.json file.
    //    $dtcg = Json::decode(file_get_contents(__DIR__ . '/../../design/design.tokens.json'));
    //    $parser = new DTCGParser();
    //    $tom = $parser->parse($dtcg);

    // Use stored Drupal config.
    $parser = new ConfigTomParser();
    $tom = $parser->parse();
    // Let other modules alter the Token Object Model.
    $this->moduleHandler()->alter('design_tokens_object_model', $tom);

    $serializer = new CssVarsSerializer();
    $styles = $serializer->serialize($tom);

    // @todo Resolve token aliases here or we can still defer it?
    // @todo Resolve sorting, the ones referencing should be after the referenced one or we don't care?
    return new Response($styles, 200, ['Content-Type' => 'text/css']);
  }

}

<?php

declare(strict_types=1);

namespace Drupal\design_tokens\tom\Parser;

use Drupal\design_tokens\tom\DesignToken;
use Drupal\design_tokens\tom\Group;
use Drupal\design_tokens\tom\RootGroup;
use Drupal\design_tokens\tom\Serializer\CssVarsSerializer;

class DTCGParser {

  public function parse(array $dtcg): RootGroup {
    $root = new RootGroup();
    $this->doParseDTCG($dtcg, '', $root);
    return $root;
  }

  protected function doParseDTCG(array $dtcg, string $prefix, Group $root): Group {
    $tuples_from_array = static fn(array $a): array => array_map(
      static fn(string $key, mixed $value): array => [$key, $value],
      array_keys($a),
      array_values($a)
    );
    return array_reduce(
      $tuples_from_array($dtcg),
      static function(Group $root, array $tuple) use ($prefix): Group {
        [$name, $group_or_token] = $tuple;
        if (!is_array($group_or_token)) {
          return $root;
        }
        $new_token_name = ($prefix ? $prefix . CssVarsSerializer::GROUP_SEPARATOR : '') . $name;
        if (array_key_exists('$value', $group_or_token)) {
          // @todo: Handle nesting?
          //       "token 1-3-2": {
          //        "$value": {
          //          "nested": {
          //            "nested again": {
          //              "$value": 123
          //            }
          //          }
          //        },
          return $root->addChild(new DesignToken(
            $new_token_name,
            $group_or_token['$type'] ?? NULL,
            $group_or_token['$value'],
            $group_or_token['$description'] ?? ''
          ));
        }
        $group = new Group($new_token_name, $group_or_token['$description'] ?? '');
        // It's a group.
        return $root->addChild(
          $this->doParseDTCG($group_or_token, $new_token_name, $group)
        );
      },
      $root,
    );
  }

}

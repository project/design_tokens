<?php

declare(strict_types=1);

namespace Drupal\design_tokens\tom;

class TomNode {

  public function __construct(
    public readonly string $name,
    public readonly ?string $type,
    public readonly ?string $description = NULL,
  ) {}

}

<?php

declare(strict_types=1);

namespace Drupal\design_tokens\ParameterFactory;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\design_tokens\Entity\DesignToken;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\Object\Error;
use Drupal\jsonrpc\ParameterDefinitionInterface;
use Drupal\jsonrpc\ParameterFactory\ParameterFactoryBase;
use JsonSchema\Validator;
use Shaper\Util\Context;
use Shaper\Validator\CollectionOfValidators;
use Shaper\Validator\InstanceofValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A factory for design token values.
 */
class DesignTokenValuesParameterFactory extends ParameterFactoryBase {

  /**
   * Constructs an EntityParameterFactory object.
   *
   * @param \Drupal\jsonrpc\ParameterDefinitionInterface $definition
   *   The parameter definition.
   * @param \JsonSchema\Validator $validator
   *   The validator to ensure the user input is valid.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(ParameterDefinitionInterface $definition, Validator $validator, protected EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($definition, $validator);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ParameterDefinitionInterface $definition, ContainerInterface $container) {
    return new static(
      $definition,
      $container->get('jsonrpc.schema_validator'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(?ParameterDefinitionInterface $parameter_definition = NULL) {
    return [
      'type' => ['array', 'null'],
      'items' => [
        'type' => 'object',
        'required' => ['id'],
        'properties' => [
          'id' => 'string',
          'value' => 'string',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getOutputValidator() {
    return new CollectionOfValidators(new InstanceofValidator(EntityInterface::class));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\jsonrpc\Exception\JsonRpcException
   */
  protected function doTransform($data, ?Context $context = NULL) {
    $token_ids = array_map(
      static fn (array $token_input) => $token_input['id'],
      $data,
    );
    $values_by_id = array_reduce(
      $data,
      fn (array $carry, array $token_input) => [...$carry, $token_input['id'] => $token_input['value'] ?? NULL],
      [],
    );
    try {
      $storage = $this->entityTypeManager->getStorage('design_token');
    }
    catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      throw JsonRpcException::fromError(Error::internalError($e->getMessage()));
    }
    $design_tokens = array_values($storage->loadMultiple($token_ids));
    return array_map(
      function (DesignToken $token) use ($values_by_id) {
        $value = $values_by_id[$token->id()];
        if (!is_null($value)) {
          $token->set('value', $value);
        }
        return $token;
      },
      $design_tokens,
    );
  }

}

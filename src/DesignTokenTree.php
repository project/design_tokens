<?php

declare(strict_types=1);

namespace Drupal\design_tokens;

use Drupal\design_tokens\Entity\DesignToken;

final class DesignTokenTree {

  /**
   * Children trees.
   *
   * @var \Drupal\design_tokens\DesignTokenTree[]
   */
  private array $subtrees = [];

  /**
   * Create a new tree.
   *
   * @param \Drupal\design_tokens\Entity\DesignToken $designToken
   *   The design token for this node in the tree.
   */
  public function __construct(public readonly DesignToken $designToken) {}

  /**
   * Adds a subtree.
   *
   * @param \Drupal\design_tokens\DesignTokenTree $subtree
   *   The subtree to add.
   */
  public function addSubtree(DesignTokenTree $subtree): void {
    $this->subtrees[] = $subtree;
  }

  /**
   * Return the subtrees.
   *
   * @return \Drupal\design_tokens\DesignTokenTree[]
   *   The subtrees.
   */
  public function getSubtrees(): array {
    return $this->subtrees;
  }

  /**
   * Get all the design tokens involved in the tree.
   *
   * @return \Drupal\design_tokens\Entity\DesignToken[]
   *   The design tokens.
   */
  public function getAllTokens(): array {
    $tokens = [$this->designToken];
    foreach ($this->subtrees as $subtree) {
      $new_tokens = array_merge($tokens, $subtree->getAllTokens());
      $tokens = array_unique([...$tokens, ...$new_tokens], SORT_REGULAR);
    }
    return $tokens;
  }

}
